import { Component, OnInit } from '@angular/core';

import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-sucursales',
  templateUrl: './sucursales.component.html',
  styleUrls: ['./sucursales.component.css']
})
export class SucursalesComponent implements OnInit {

		public sucursales: any[] = [];
	    public paginacion = [];

	    constructor(private apiService: ApiService, private alertService: AlertService){ }

		ngOnInit() {
	        this.loadAll();
	    }

	    private loadAll() {
	        this.apiService.getAll('sucursales').subscribe(sucursales => { 
	            this.sucursales = sucursales;
	            this.paginacion = [];
	            for (let i = 0; i < sucursales.last_page; i++) { this.paginacion.push(i+1); }
	        }, error => {this.alertService.error(error); });
	    }

	    private search($text){
	    	if($text) {
		    	this.apiService.read('sucursales/buscar/', $text).subscribe(sucursales => { 
		    	    this.sucursales = sucursales;
		    	    this.paginacion = [];
		    	    for (let i = 0; i < sucursales.last_page; i++) { this.paginacion.push(i+1); }
		    	}, error => {this.alertService.error(error); });
	    	}
	    }

	    private delete($id) {
	        if (confirm('¿Desea eliminar el Registro?')) {
	            this.apiService.delete('cliente/', $id) .subscribe(data => {
	                for (let i in this.sucursales['data']) {
	                    if (this.sucursales['data'][i].id == data.id )
	                        this.sucursales['data'].splice(i, 1);
	                }
	            }, error => {this.alertService.error(error); });
	                   
	        }

	    }

	    private setPaginacion(page:number) {
	        this.apiService.getAll('sucursales?page='+ page).subscribe(sucursales => { this.sucursales = sucursales; });
	    }

}
