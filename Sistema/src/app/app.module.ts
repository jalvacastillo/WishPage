import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { routing } from './app.routing';

import { AlertComponent } from './shared/alert/alert.component';
import { AuthGuard } from './auth.guard';
import { AlertService } from './services/alert.service';
import { AuthService } from './services/auth.service';
import { ApiService } from './services/api.service';

import { LoginComponent } from './login/login.component';
import { DashComponent } from './dash/dash.component';

import { HeaderComponent } from './shared/header/header.component';
import { AsideComponent } from './shared/aside/aside.component';
import { FooterComponent } from './shared/footer/footer.component';
import { ClientesComponent } from './dash/clientes/clientes.component';
import { ClienteComponent } from './dash/clientes/cliente/cliente.component';
import { PerfilComponent } from './dash/admin/perfil/perfil.component';
import { EmpresaComponent } from './dash/admin/empresa/empresa.component';
import { SucursalesComponent } from './dash/admin/sucursales/sucursales.component';
import { UsuariosComponent } from './dash/admin/usuarios/usuarios.component';

@NgModule({
  declarations: [
    AppComponent,
    AlertComponent,
    LoginComponent,
    DashComponent,
    HeaderComponent,
    AsideComponent,
    FooterComponent,
    ClientesComponent,
    ClienteComponent,
    PerfilComponent,
    EmpresaComponent,
    SucursalesComponent,
    UsuariosComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  providers: [AuthGuard, AlertService, AuthService, ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
