import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { PerfilComponent } from './dash/admin/perfil/perfil.component';

// Admin
import { EmpresaComponent } from './dash/admin/empresa/empresa.component';
import { SucursalesComponent } from './dash/admin/sucursales/sucursales.component';
import { UsuariosComponent } from './dash/admin/usuarios/usuarios.component';

import { DashComponent } from './dash/dash.component';

import { ClientesComponent } from './dash/clientes/clientes.component';
import { ClienteComponent } from './dash/clientes/cliente/cliente.component';

import { AuthGuard } from './auth.guard';

const appRoutes: Routes = [
    { path: 'login', component: LoginComponent },
    
    { path: 'admin/perfil', component: PerfilComponent },
    { path: 'admin/empresa', component: EmpresaComponent },
    { path: 'admin/sucursales', component: SucursalesComponent },
    { path: 'admin/usuarios', component: UsuariosComponent },

    { path: 'dashboard', component: DashComponent, canActivate: [AuthGuard] },
    { path: 'clientes', component: ClientesComponent, canActivate: [AuthGuard] },
    { path: 'cliente/:id', component: ClienteComponent, canActivate: [AuthGuard] },

    // otherwise redirect to home
    { path: '**', redirectTo: 'dashboard' }
];

export const routing = RouterModule.forRoot(appRoutes);