<div class="navbar-fixed">
    <nav class="transparent">
        <div class="container nav-wrapper">
          <a href="{{ route('home') }}" class="brand-logo hide-on-med-and-down">
            <img src="{{ asset('img/logo.png') }}" alt="" class="responsive-img">
          </a>
          <a href="#" data-target="mobile-demo" class="sidenav-trigger">
            <i class="material-icons">menu</i>
          </a>
          <div id="nav-buscador">@include('partials.buscador')</div>
          {{-- <ul class="right hide-on-med-and-down"> --}}
          <ul class="right">
            <li>
              <button class="waves-effect waves-light btn">Registrarme</button>
            </li>
          </ul>
        </div>
    </nav>
</div>
<ul class="sidenav" id="mobile-demo">
    <li> <a href="" class="tooltipped" data-tooltip="Carrito"> <i class="material-icons">shopping_cart</i> </a> </li>
    <li> <a href="" class="tooltipped" data-tooltip="Cuenta"> <i class="material-icons">account_circle</i> </a> </li>
</ul>