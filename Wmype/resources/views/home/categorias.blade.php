<div id="intro" class="section scrollspy">
    <div class="container">
        
        <div class="row">

        @foreach ($categorias as $categoria)
          <div class="col s12 m4">
            <div class="card">
              <div class="card-image">
                <img src="https://picsum.photos/id/1/200/150">
              </div>
              <div class="card-action center">
                <a href="#" class="red-text">{{ $categoria->nombre }}</a>
              </div>
            </div>
          </div>
        @endforeach

        </div>

    </div>
</div>