<div class="carousel max carousel-slider center carousel-header">
    <div class="carousel-fixed-item center">
        @include('partials.buscador')
    </div>
    <div class="carousel-item red white-text" href="#one!">
        <h2>Tienda MYPE</h2>
        <p class="white-text">Productos Salvadoreños</p>
        @if (isset($parametro))
            <h6 class="white-text">
                Total de Registros para la busqueda <b>"{{$parametro}}"</b> : <b>{{$productos->count()}}</b>
            </h6>
        @endif
    </div>
</div>