@extends('layout')

@section('content')

    @include('home.header')
    
    @if ($empresas->total() > 0)
        @include('home.empresas')
    @else
        <div class="section scrollspy" id="work">
            <div class="container">
                <div class="row">
                    <div class="col s12 center-align" style="min-height: 330px;">
                        <h4>Aún no hay Empresas</h4>
                        <img src="{{ asset('/img/empy.gif') }}" alt="" width="300px">

                        @if (Route::is('buscador'))
                            <p><a href="{{ route('home') }}" class="btn red">Ir al inicio</a></p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    @endif

@endsection