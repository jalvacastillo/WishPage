<div class="section scrollspy productos" ng-controller="ProductoCtrl">
    <div class="container">
        <div class="row">
        @foreach ($empresas as $empresa)
        <a href="{{ route('empresa', $empresa->slug) }}">
            <div class="col s6 m4 l3" style="min-height: 440px;">
                @include('partials.empresa')
            </div>
        </a>
        @endforeach
        </div>

        <div class="row center">
            {{$empresas->links('partials.pagination')}}
        </div>
    </div>
</div>