<div class="container-fluid grey lighten-3">
	<div class="row">
		<div class="col m5 s10 info-img">
			<img src="{{ asset('img/info.png') }}" class="info-img">
		</div>
		<div class="col m7 s10 center-align">
			<h1 class="valign">Sobre nosotros</h1>
			<h2>{{$empresa->descripcion}}</h2>
		</div>
	</div>
</div>