<div class="container">
  <div class="row">
    <h2 class="text-center">Productos</h2>
    
    @foreach($empresa->productos as $producto)
    <div class="col s6 m4">
    <h2 class="header">{{$producto->nombre}}</h2>
    <div class="card horizontal">
      <div class="card-image">
        <img src="{{ asset('img/'.$producto->img) }}">
      </div>
      <div class="card-stacked">
        <div class="card-content">
          <p>{{$producto->descripcion}}</p>
        </div>
        <div class="card-action">
          <a href="#">This is a link</a>
        </div>
      </div>
    </div>
  </div>
  @endforeach()
</div>
</div>
