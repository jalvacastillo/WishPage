<div class="container">
<div class="row">
      <div class="col-md-12 center">
      	<div class="panel panel-default">
          <div class="userprofile social">
            <div class="userpic"> <img src="{{ asset('img/'.$empresa->logo) }}" alt="" class="userpicimg"> </div>
            <h3 class="username">{{$empresa->nombre}}</h3>
            <p>{{$empresa->municipio}}, {{$empresa->departamento}}</p>
            <div class="socials center"> <a href="" class="btn btn-circle btn-primary ">
            <i class="fa fa-facebook circle blue darken-4"></i></a> <a href="" class="waves-effect waves-light btn social facebook">
            <i class="fa fa-google-plus"></i></a> <a href="" class="btn btn-circle btn-info ">
            <i class="fa fa-twitter"></i></a> <a href="" class="btn btn-circle btn-warning "><i class="fa fa-envelope"></i></a>
            </div>
          </div>
        
        </div>
      </div>
     </div>
    </div>