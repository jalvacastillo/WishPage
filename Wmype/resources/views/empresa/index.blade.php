@extends('layout')

@section('content')
    
    @include('empresa.encabezado')
    @include('empresa.productos')
    @include('empresa.descripcion')

@endsection