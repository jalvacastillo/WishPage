<!doctype html>
<html lang="en" ng-app="app">
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="/favicon.png">
    <link rel="icon" type="image/ico" href="/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Tavo - Tienda de productos</title>

    <meta name='viewport'content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'/>

    <meta name="description" content="Sistema para gasolineras.">
    <meta name="keywords" content="Sistema, gasolineras, sistema en linea, el salvador">
    <meta name="author" content="Jesus Alvarado">

    <meta property="og:url"                content="http://wgas.websis.me" />
    <meta property="og:type"               content="product" />
    <meta property="og:title"              content="Wgas - Sistema para gasolineras." />
    <meta property="og:description"        content="Automatiza el proceso de facturación, permitiendo además monitorear el flujo de efectivo y llevando el control del inventario tanto de gasolina como de productos." />
    <meta property="og:image"              content="{{ asset('img/logo.png') }}" />

    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
    <link rel="alternate" hreflang="es-SV" href="http://websis.me/" />

    <!-- CSS Files -->
    <link href="{{ asset('/css/materialize.min.css') }}" type="text/css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="{{ asset('/css/style.css') }}" type="text/css" rel="stylesheet">

    <script src="{{ asset('js/angular.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('app/controller.js') }}" type="text/javascript"></script>

</head>

<body id="top" class="scrollspy" ng-controller="MainCtrl">

    <!-- Pre Loader -->
    <div id="loader-wrapper">
        <div id="loader"></div>
     
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
     
    </div>
    @include('navbar')

    @yield('content')

    @include('footer')
    
    <!--   Core JS Files   -->
    <script src="{{ asset('js/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/modernizr.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/materialize.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/app.js') }}" type="text/javascript"></script>

    <script>
      angular.module("app").constant("CSRF_TOKEN", '{{ csrf_token() }}');
    </script>

</body>
</html>
