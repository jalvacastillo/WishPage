@if($errors)
   @foreach ($errors->all() as $error)
   <div class="col s12">
       <div class="alert">
           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
           <strong>Hoops!</strong> {{ $error }}
       </div>
   </div>
  @endforeach
@endif