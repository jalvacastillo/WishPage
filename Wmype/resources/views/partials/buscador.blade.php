<form action="{{ route('buscador') }}" method="GET" autocomplete="off">

    {!! csrf_field() !!}
    
    <div class="row" id="buscador">
        <input name="buscador" placeholder="¿Qué quieres pedir?" type="search" class="validate">
    </div>

</form>