<div class="card hoverable">
    <div class="card-image">
        <img class="activator" src="{{ asset('img/'.$empresa->logo) }}">
        <a  style="right: 5px;" class="btn-floating halfway-fab waves-effect waves-light tooltipped" data-tooltip="Agregar al carrito">
            <i class="material-icons">add</i>
        </a>
        <a id="favorito{{$empresa->id}}" ng-click="addFavorito({{$empresa->id}})" style="right: 50px;" class="btn-floating halfway-fab waves-effect waves-light tooltipped" data-tooltip="Agregar a favoritos">
            <i class="material-icons">favorite</i>
        </a>
    </div>
    <div class="card-content">
      <span class="card-title">{{($empresa->nombre) }}</span>
     {{--  <h6 class="red-text">${{ number_format($producto->precio,2) }}</h6> --}}
     
    </div>
</div>