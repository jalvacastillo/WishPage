<div class="carousel min carousel-slider carousel-header center">
    <div class="carousel-item red white-text">
        <h5>
            <a class="white-text" href="{{ route('home') }}">Tienda</a>
            <i class="material-icons">chevron_right</i>
            {{ $title }}
        </h5>
    </div>
</div>
