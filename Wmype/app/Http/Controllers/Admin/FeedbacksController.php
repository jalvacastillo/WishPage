<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\FeedbackRequest;
use App\Models\Tienda\Feedback;

class FeedbacksController extends Controller
{
    
    public function index() {
       
        $feedbacks = Feedback::orderBy('id', 'asc')->get();

        return Response()->json($feedbacks, 200);

    }


    public function read($id) {

        $feedback = Feedback::findOrFail($request->id);
        return Response()->json($feedback, 200);

    }


    public function store(FeedbackRequest $request)
    {
        if($request->id){
            $feedback = Feedback::findOrFail($request->id);
        }
        else{
            $feedback = new Feedback;
        }

        if ($request->hasFile('file')) {
                
            $file = $request->file;
            $ruta = public_path() . '/imgs/feedbacks';
            if ($feedback->img) { \File::delete($ruta . $feedback->img); }
            $file->move($ruta, $request->img);
        } 
        
        $feedback->fill($request->all());
        $feedback->save();

        return Response()->json($feedback, 200);

    }

    public function delete($id)
    {
        $feedback = Feedback::findOrFail($id);
        $feedback->delete();

        return Response()->json($feedback, 201);

    }


}
