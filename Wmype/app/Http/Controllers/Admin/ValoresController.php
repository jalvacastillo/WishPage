<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ValorRequest;
use App\Models\Admin\Valor;

class ValoresController extends Controller
{
    

    public function index() {
       
        $valores = Valor::orderBy('nombre','asc')->get();

        return Response()->json($valores, 200);

    }


    public function read($id) {

        $valor = Valor::findOrFail($request->id);
        return Response()->json($valor, 200);

    }


    public function store(ValorRequest $request)
    {
        if($request->id){
            $valor = Valor::findOrFail($request->id);
        }
        else{
            $valor = new Valor;
        }
        
        $valor->fill($request->all());
        $valor->save();

        return Response()->json($valor, 200);

    }

    public function delete($id)
    {
        $valor = Valor::findOrFail($id);
        $valor->delete();

        return Response()->json($valor, 201);

    }


}
