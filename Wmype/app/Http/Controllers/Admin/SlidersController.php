<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SliderRequest;
use App\Models\Tienda\Slider;

class SlidersController extends Controller
{
    
    public function index() {
       
        $sliders = Slider::orderBy('orden', 'asc')->get();

        return Response()->json($sliders, 200);

    }


    public function read($id) {

        $slider = Slider::findOrFail($request->id);
        return Response()->json($slider, 200);

    }


    public function store(SliderRequest $request)
    {
        if($request->id){
            $slider = Slider::findOrFail($request->id);
        }
        else{
            $slider = new Slider;
        }

        if ($request->hasFile('file')) {
                
            $file = $request->file;
            $ruta = public_path() . '/imgs/sliders';
            if ($slider->img) { \File::delete($ruta . $slider->img); }
            $file->move($ruta, $request->img);
        } 

        $slider->fill($request->all());
        $slider->save();        

        return Response()->json($slider, 200);

    }

    public function delete($id)
    {
        $slider = Slider::findOrFail($id);
        $slider->delete();

        return Response()->json($slider, 201);

    }


}
