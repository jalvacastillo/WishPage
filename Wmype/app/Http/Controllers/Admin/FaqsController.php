<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\FaqRequest;
use App\Models\Tienda\Faq;

class FaqsController extends Controller
{
    
    public function index() {
       
        $faqs = Faq::orderBy('orden', 'asc')->get();

        return Response()->json($faqs, 200);

    }


    public function read($id) {

        $faq = Faq::findOrFail($request->id);
        return Response()->json($faq, 200);

    }


    public function store(FaqRequest $request)
    {
        if($request->id){
            $faq = Faq::findOrFail($request->id);
        }
        else{
            $faq = new Faq;
        }

        $faq->fill($request->all());
        $faq->save();        

        return Response()->json($faq, 200);

    }

    public function delete($id)
    {
        $faq = Faq::findOrFail($id);
        $faq->delete();

        return Response()->json($faq, 201);

    }


}
