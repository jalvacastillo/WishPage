<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\TiendaRequest;
use App\Models\Tienda\Tienda;

class TiendaController extends Controller
{
    

    public function index() {
       
        $tiendas = Tienda::orderBy('id','dsc')->paginate(7);

        return Response()->json($tiendas, 200);

    }


    public function read($id) {

        $tienda = Tienda::findOrFail($id);
        return Response()->json($tienda, 200);

    }

    public function store(TiendaRequest $request)
    {
        if($request->id){
            $tienda = Tienda::findOrFail($request->id);
        }
        else{
            $tienda = new Tienda;
        }

        if ($request->hasFile('file_logo')) {
                
            $file_logo = $request->file_logo;
            $ruta = public_path() . '/imgs';
            if ($tienda->logo) { \File::delete($ruta . $tienda->logo); }
            $file_logo->move($ruta, $request->logo);
        } 

        if ($request->hasFile('file_media')) {
                
            $file_media = $request->file_media;
            $ruta = public_path() . '/imgs/media';
            if ($tienda->media_img) { \File::delete($ruta . $tienda->media_img); }
            $file_media->move($ruta, $request->media_img);
        } 
        
        $tienda->fill($request->all());
        $tienda->save();

        return Response()->json($tienda, 200);

    }

    public function delete($id)
    {
        $tienda = Tienda::findOrFail($id);
        $tienda->delete();

        return Response()->json($tienda, 201);

    }

}
