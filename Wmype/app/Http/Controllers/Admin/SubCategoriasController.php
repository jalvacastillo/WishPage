<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SubCategoriaRequest;
use App\Models\Admin\SubCategoria;

class SubCategoriasController extends Controller
{
    

    public function index() {
       
        $subcategorias = SubCategoria::all();

        return Response()->json($subcategorias, 200);

    }


    public function read($id) {

        $subcategoria = SubCategoria::findOrFail($request->id);
        return Response()->json($subcategoria, 200);

    }


    public function store(SubCategoriaRequest $request)
    {
        if($request->id){
            $subcategoria = SubCategoria::findOrFail($request->id);
        }
        else{
            $subcategoria = new SubCategoria;
        }

        if ($request->hasFile('file')) {
                
            $file = $request->file;
            $ruta = public_path() . '/imgs/categorias';
            if ($subcategoria->img) { \File::delete($ruta . $subcategoria->img); }
            $file->move($ruta, $request->img);
        } 
        
        $subcategoria->fill($request->all());
        $subcategoria->save();

        return Response()->json($subcategoria, 200);

    }

    public function delete($id)
    {
        $subcategoria = SubCategoria::findOrFail($id);
        $subcategoria->delete();

        return Response()->json($subcategoria, 201);

    }


}
