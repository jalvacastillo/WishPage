<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CategoriaRequest;
use App\Models\Admin\Categoria;

class CategoriasController extends Controller
{
    
    public function index() {
       
        $categorias = Categoria::orderBy('id', 'desc')->with('subcategorias')->get();

        return Response()->json($categorias, 200);

    }


    public function read($id) {

        $categoria = Categoria::findOrFail($request->id);
        return Response()->json($categoria, 200);

    }


    public function store(CategoriaRequest $request)
    {
        if($request->id){
            $categoria = Categoria::findOrFail($request->id);
        }
        else{
            $categoria = new Categoria;
        }

        if ($request->hasFile('file')) {
                
            $file = $request->file;
            $ruta = public_path() . '/imgs/categorias';
            if ($categoria->img) { \File::delete($ruta . $categoria->img); }
            $file->move($ruta, $request->img);
        } 
        
        $categoria->fill($request->all());
        $categoria->save();

        return Response()->json($categoria, 200);

    }

    public function delete($id)
    {
        $categoria = Categoria::findOrFail($id);
        $categoria->delete();

        return Response()->json($categoria, 201);

    }


}
