<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use JWTAuth;
use NumeroALetras;
use App\Models\Admin\Empresa;
use App\Models\Admin\Bodega;
use App\Models\Admin\Corte;
use App\Models\Ventas\Venta;
use App\Models\Ventas\DevolucionVenta;
use App\Models\Ventas\Detalle;
use App\Models\Inventario\Requisicion;

class ReportesController extends Controller
{
    

    public function facturacion($id) {
       
        $venta = Venta::where('id', $id)->with('detalles', 'cliente')->first();
        $empresa = Empresa::find(1);

        $venta->total_letras = NumeroALetras::convertir(number_format($venta->total, 2), 'Dolares', '/100');

        if ($venta) {
            if ($venta->tipo_transaccion == 'Factura') {

                return view('reportes.factura', compact('venta', 'empresa'));
            }
            elseif ($venta->tipo_transaccion == 'Credito Fiscal') {

                return view('reportes.credito', compact('venta', 'empresa'));
            }
            else{
                return "Venta sin tipo";
            }
        }else{
            return "Venta no existe";       
        }

    }

    public function devolucion($id) {
       
        $venta = DevolucionVenta::where('id', $id)->with('detalles', 'cliente')->first();

        if ($venta) {
            if ($venta->tipo_transaccion == 'Factura') {

                return view('reportes.factura', compact('venta'));
            }
            elseif ($venta->tipo_transaccion == 'Credito Fiscal') {

                return view('reportes.credito', compact('venta'));
            }
            else{
                return "Devolucion";
            }
        }else{
            return "Devolucion";       
        }

    }

    public function requisicion($id) {

        $requisicion = Requisicion::where('id', $id)->with('detalles')->first();
        $empresa = Empresa::find(1);

        if ($requisicion) {
            $reportes = \PDF::loadView('reportes.requisicion', compact('requisicion', 'empresa'));
            return $reportes->stream();
        }else{
            return "No entontrado";
        }

    }

    public function requisicionCompra(Request $request) {

        $requisicion = $request;
        $requisicion = Requisicion::where('id', 1)->with('detalles')->first();
        $empresa = Empresa::find(1);

        // return $requisicion;

        if ($requisicion) {
            $reportes = \PDF::loadView('reportes.requisicion', compact('requisicion', 'empresa'));
            return $reportes->download();
        }else{
            return "No entontrado";
        }

    }

    public function bodegas($id) {

        $bodega = Bodega::where('id', $id)->with('productos')->first();
        $bodega->created_at = Carbon::now();
        $bodega->usuario = JWTAuth::parseToken()->authenticate()->name;
        $empresa = Empresa::find(1);

        if ($bodega) {
            $reportes = \PDF::loadView('reportes.bodegas', compact('bodega', 'empresa'));
            return $reportes->stream();
        }else{
            return "No entontrado";
        }

    }

    public function corte($id) {

        $corte = Corte::where('id', $id)->first();
        $empresa = Empresa::find(1);

        if (!$corte->final) {
            $corte->final = Carbon::now(config('app.timezone'));
        }

        $ventas = Venta::where('usuario_id', $corte->usuario()->first()->id)->whereBetween('created_at', [$corte->inicio, $corte->final] )->get();
        $devolucion = DevolucionVenta::where('usuario_id', $corte->usuario()->first()->id)->whereBetween('created_at', [$corte->inicio, $corte->final] )->get();

        // Por Forma de Pago
        $corte->cantidad_ventas     = $ventas->count();
        $corte->ventas_efectivo     = $ventas->where('forma_de_pago','Efectivo')->sum('total');
        $corte->ventas_tarjeta      = $ventas->where('forma_de_pago','Tarjeta')->sum('total');
        $corte->ventas_vale         = $ventas->where('forma_de_pago','Vale')->sum('total');
        $corte->ventas_cheque       = $ventas->where('forma_de_pago','Cheque')->sum('total');
        $corte->ventas_versatec     = $ventas->where('forma_de_pago','Versatec')->sum('total');

        // Galonaje
        $galones_dia                = Detalle::whereIn('producto_id', [1,2,3])->whereBetween('created_at', [$corte->inicio, $corte->final] )->get();
        $corte->galones_dia_super   = $galones_dia->where('producto_id', 1)->sum('cantidad');
        $corte->galones_dia_regular = $galones_dia->where('producto_id', 2)->sum('cantidad');
        $corte->galones_dia_diesel  = $galones_dia->where('producto_id', 3)->sum('cantidad');
        $corte->galones_dia         = $galones_dia->sum('cantidad');

        $corte->devoluciones          = $devolucion->sum('total');

        if ($corte) {
            $reportes = \PDF::loadView('reportes.corte', compact('corte', 'empresa'));
            return $reportes->stream();
        }else{
            return "No entontrado";
        }

    }


}
