<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UsuarioRequest;
use App\User as Usuario;
use Auth;

class UsuariosController extends Controller
{
    

    public function index() {
       
        $usuarios = Usuario::orderBy('id','dsc')->paginate(7);

        return Response()->json($usuarios, 200);

    }

    public function cliente() {
       
        $usuario = Usuario::where('id', 2)->with('direcciones')->first();

        return Response()->json($usuario, 200);

    }


    public function read($id) {
        
        $usuario = Usuario::findOrFail($id);

        return Response()->json($usuario, 200);
    }

    public function filter($campo, $valor) {
        
        $usuario = Usuario::where($campo, $valor)->paginate(15);

        return Response()->json($usuario, 200);
    }

    public function search($txt) {

        $usuarios = Usuario::where('name', 'like' ,'%' . $txt . '%')->paginate(7);
        return Response()->json($usuarios, 200);

    }


    public function store(UsuarioRequest $request)
    {
        if($request->id){
            $usuario = Usuario::findOrFail($request->id);
        }
        else{
            $usuario = new Usuario;
        }


        if ($request->password) {
            $request['password'] = \Hash::make($request->password);
        }
        
        $usuario->fill($request->all());
        $usuario->save();

        return Response()->json($usuario, 200);


    }

    public function delete($id)
    {
       
        $usuario = Usuario::findOrFail($id);
        $usuario->delete();

        return Response()->json($usuario, 201);

    }

}
