<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AtributoRequest;
use App\Models\Admin\Atributo;

class AtributosController extends Controller
{
    

    public function index() {
       
        $atributos = Atributo::orderBy('id','desc')->with('valores')->get();

        return Response()->json($atributos, 200);

    }


    public function read($id) {

        $atributo = Atributo::findOrFail($request->id);
        return Response()->json($atributo, 200);

    }


    public function store(AtributoRequest $request)
    {
        if($request->id){
            $atributo = Atributo::findOrFail($request->id);
        }
        else{
            $atributo = new Atributo;
        }
        
        $atributo->fill($request->all());
        $atributo->save();

        return Response()->json($atributo, 200);

    }

    public function delete($id)
    {
        $atributo = Atributo::findOrFail($id);
        $atributo->delete();

        return Response()->json($atributo, 201);

    }


}
