<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\Productos\Producto;
use App\Models\Productos\Comentario;
use App\Models\Admin\Categoria;

class ProductosController extends Controller
{
    public function index()
    {   
        $productos = Producto::orderBy('id', 'desc')->paginate(12);
        $categorias = Categoria::all();

        return view('tienda.index', compact('productos', 'categorias'));
    }

    public function producto(Request $request, $slug)
    {   
        $producto = Producto::with(array('imagenes' => function($query) {
                                    $query->orderBy('orden', 'asc');
                                }))
                                ->with('atributos.valores')
                                ->with(array('comentarios' => function($query) {
                                    $query->orderBy('created_at', 'desc')->take(5);
                                }))
                                ->where('slug', $slug)->firstOrFail();

        if ($request->ajax()) {
            return response()->json(['producto' => $producto]);
        }
                                
        return view('producto.index', compact('producto'));
    }

    public function comentarios(Request $request, $slug)
    {   
        $producto = Producto::where('slug', $slug)->firstOrFail();

        $comentarios = Comentario::where('producto_id', $producto->id)->orderBy('created_at', 'desc')->paginate(12);

        if ($request->ajax()) {
            return response()->json(['producto' => $producto, 'comentarios' => $comentarios]);
        }
                                
        return view('tienda.producto.comentarios', compact('producto', 'comentarios'));
    }

    public function comentario(Request $request)
    {
        $request['cliente_id'] = Auth::user()->id;
        
        $request->validate([
            'producto_id'   => 'required',
            'nota'          => 'required',
            'descripcion'   => 'required',
            'cliente_id'    => 'required',
        ]);

        if($request->id){
            $comentario = Comentario::findOrFail($request->id);
        }
        else{
            $comentario = new Comentario;
        }
        
        $comentario->fill($request->all());
        $comentario->save();

        if ($request->ajax()) {
            return Response()->json($comentario, 200);
        }
        return view('tienda.producto.index', compact('producto'));

    }

}
