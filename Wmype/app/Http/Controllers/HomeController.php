<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Admin\Categoria;
use App\Models\Productos\Producto;
use App\Models\Empresa\Empresa;

class HomeController extends Controller
{
    public function index()
    {   
        $empresas = Empresa::orderBy('id', 'desc')->paginate(20);
        return view('home.index', compact('empresas'));
    }

    public function empresa($slug)
    {   
        $empresa = Empresa::where('slug', $slug)->firstOrFail();
        $productos = $empresa->with('productos')->paginate(6);

        return view('empresa.index', compact('empresa', 'productos'));
    }

    public function buscador(Request $request)
    {   
        $parametro = $request->buscador;

        $productos = Producto::
                            where('nombre', 'like', '%' . $request->buscador . '%')
                            ->orderBy('id', 'desc')
                            ->paginate(20);

        return view('home.index', compact('productos', 'parametro'));
    }

}
