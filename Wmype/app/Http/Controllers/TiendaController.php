<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Productos\Producto;
use App\Models\Admin\Categoria;
use App\Models\Admin\SubCategoria;

class TiendaController extends Controller
{
    public function index() {
        $productos  = Producto::orderBy('id', 'desc')->paginate(12);
        $categorias = Categoria::all();

        return view('tienda.index', compact('productos', 'categorias'));
    }
    
    public function categorias(){
        $productos  = Producto::orderBy('id', 'desc')->paginate(12);

        $categorias = Categoria::all();

        return view('tienda.categorias', compact('productos', 'categorias'));
    }

    public function categoria($slug){

        $productos  = Producto::with('subcategoria')->whereHas('subcategoria', function($query) use ($slug){
                                    $query->with('categoria')
                                        ->whereHas('categoria', function($query) use ($slug){
                                            $query->where('slug', $slug);
                                        });
                                })->orderBy('id', 'desc')->paginate(12);

        $categoria      = Categoria::with('subcategorias')->where('slug', $slug)->first();
        $subcategorias  = $categoria->subcategorias;

        return view('tienda.index', compact('productos', 'categoria', 'subcategorias'));
    }

    public function subcategoria($categoria_slug, $slug){

        $productos  = Producto::with('subcategoria')->whereHas('subcategoria', function($query) use ($categoria_slug, $slug){
                                    $query->with('categoria')->where('slug', $slug)
                                        ->whereHas('categoria', function($query) use ($categoria_slug){
                                            $query->where('slug', $categoria_slug);
                                        });
                                })->orderBy('id', 'desc')->paginate(12);

        $categoria      = Categoria::where('slug', $categoria_slug)->first();
        $subcategoria   = SubCategoria::where('slug', $slug)->first();
        $subcategorias  = SubCategoria::with('categoria')
                                    ->wherehas('categoria', function($q) use ($categoria_slug){
                                        $q->where('slug', $categoria_slug);
                                    })->get();

        return view('tienda.index', compact('productos', 'categoria', 'subcategoria', 'subcategorias'));
    }

    public function nuevosEstilos(){
        $productos  = Producto::nuevos()->orderBy('id', 'desc')->paginate(12);
        $categorias = Categoria::all();

        return view('tienda.index', compact('productos', 'categorias'));
    }

    public function nuevosEstilosCat($slug){
        $productos  = Producto::nuevos()
                                ->with('subcategoria')->whereHas('subcategoria', function($query) use ($slug){
                                    $query->where('slug', $slug);
                                })
                                ->orderBy('id', 'desc')->paginate(12);
        $categorias = Categoria::all();

        return view('tienda.index', compact('productos', 'categorias'));
    }

    public function exclusivo(){
        $productos  = Producto::exclusivos()->orderBy('id', 'desc')->paginate(12);
        $categorias = Categoria::all();

        return view('tienda.index', compact('productos', 'categorias'));
    }

    public function ofertas(){
        $productos  = Producto::ofertas()->orderBy('id', 'desc')->paginate(12);
        $categorias = Categoria::all();

        return view('tienda.index', compact('productos', 'categorias'));
    }

    public function tendencia(){
        $productos  = Producto::orderBy('id', 'desc')->paginate(12);
        $categorias = Categoria::all();

        return view('tienda.index', compact('productos', 'categorias'));
    }



    public function filtrar(Request $request){

        // return $request;

        $productos  = Producto::when($request->nombre, function($query) use ($request){
                                    return $query->where('nombre', 'like', '%'.  $request->nombre . '%');
                                })
                                ->when($request->orden, function($query) use ($request){
                                    return $query->orderBy('precio', $request->orden);
                                })
                                ->paginate(12);

        $categorias = Categoria::all();

        return view('tienda.index', compact('productos', 'categorias'));
    }


}
