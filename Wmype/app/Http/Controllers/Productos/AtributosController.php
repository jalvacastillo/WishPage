<?php

namespace App\Http\Controllers\Productos;

use App\Http\Controllers\Controller;
use App\Http\Requests\Productos\AtributoRequest;

use App\Models\Productos\Atributo;

class AtributosController extends Controller
{
    

    public function index() {
       
        $atributos = Atributo::orderBy('created_at','dsc')->with('valores')->paginate(7);

        return Response()->json($atributos, 200);

    }


    public function read($id) {

        $atributo = Atributo::findOrFail($id);
        return Response()->json($atributo, 200);

    }


    public function store(AtributoRequest $request)
    {
        if($request->id){
            $atributo = Atributo::findOrFail($request->id);
        }
        else{
            $atributo = new Atributo;
        }     

        $atributo->fill($request->all());
        $atributo->save();        

        return Response()->json($atributo, 200);

    }

    public function delete($id)
    {
        $atributo = Atributo::findOrFail($id);
        $atributo->delete();

        return Response()->json($atributo, 201);

    }


    public function search($txt) {

        $atributos = Atributo::whereHas('cliente', function($query) use ($txt)
                    {
                        $query->where('nombre', 'like' ,'%' . $txt . '%');

                    })->paginate(7);

        return Response()->json($atributos, 200);

    }


}
