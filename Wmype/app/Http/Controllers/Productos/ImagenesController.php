<?php

namespace App\Http\Controllers\Productos;

use App\Http\Controllers\Controller;
use App\Http\Requests\Productos\ImagenRequest;

use App\Models\Productos\Imagen;

class ImagenesController extends Controller
{
    

    public function index() {
       
        $imagenes = Imagen::orderBy('created_at','dsc')->paginate(7);

        return Response()->json($imagenes, 200);

    }


    public function store(ImagenRequest $request)
    {

        if($request->id){
            $imagen = Imagen::findOrFail($request->id);
        }
        else{
            $imagen = new Imagen;
        }

        if ($request->hasFile('file')) {
                
            $file = $request->file;
            $ruta = public_path() . '/imgs/productos/imagenes';
            if ($imagen->img) { \File::delete($ruta . $imagen->img); }
            $file->move($ruta, $request->img);
        } 

        $imagen->fill($request->all());
        $imagen->save();        

        return Response()->json($imagen, 200);

    }

    public function delete($id)
    {
        $imagen = Imagen::findOrFail($id);
        $imagen->delete();

        return Response()->json($imagen, 201);

    }


}
