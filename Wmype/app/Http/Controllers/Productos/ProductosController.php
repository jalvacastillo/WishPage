<?php

namespace App\Http\Controllers\Productos;

use App\Http\Controllers\Controller;
use App\Http\Requests\Productos\ProductoRequest;
use Illuminate\Http\Request;

use App\Models\Productos\Producto;

class ProductosController extends Controller
{
    

    public function index() {
       
        $productos = Producto::orderBy('created_at','dsc')->paginate(7);

        return Response()->json($productos, 200);

    }

    public function read($id) {

        $producto = Producto::where('id', $id)->with(array('imagenes' => function($query) {
                                                        $query->orderBy('orden', 'asc');
                                                    }))
                                            ->with('atributos.valores')
                                            ->with(array('comentarios' => function($query) {
                                                        $query->orderBy('created_at', 'desc');
                                                    }))->first();
        return Response()->json($producto, 200);

    }

    public function search($txt) {

        $productos = Producto::where('categoria_id', '!=', 1)->where('nombre', 'like' ,'%' . $txt . '%')->paginate(7);
        return Response()->json($productos, 200);

    }

    public function filter(Request $request) {

            // $star = $request->fecha_ini . ' ' . $request->hora_ini;
            // $end = $request->fecha_fin . ' ' . $request->hora_fin;

            $productos = Producto::where('categoria_id', '!=', 1)->with('bodegas')//->whereBetween('created_at', [$star, $end])
                                ->when($request->categoria_id, function($query) use ($request){
                                    return $query->where('categoria_id', $request->categoria_id);
                                })
                                ->when($request->stock_bodega, function($query) use ($request){
                                    return $query->whereHas('bodegas', function($query){
                                        return $query->where('bodega_id', 1)->whereRaw('stock <= stock_min');
                                    });
                                })
                                ->when($request->stock_venta, function($query) use ($request){
                                    return $query->whereHas('bodegas', function($query){
                                        return $query->where('bodega_id', 2)->whereRaw('stock <= stock_min');
                                    });
                                })
                                ->orderBy('id','dsc')->paginate(100000);

            return Response()->json($productos, 200);
    }

    public function store(ProductoRequest $request)
    {
        if($request->id){
            $productos = Producto::findOrFail($request->id);
        }
        else{
            $productos = new Producto;
        }
        
        $productos->fill($request->all());
        $productos->save();

        return Response()->json($productos, 200);

    }

    public function delete($id)
    {
        $producto = Producto::findOrFail($id);
        $producto->delete();

        return Response()->json($producto, 201);

    }


    public function cardex(Request $request){

        $id = $request->producto_id;
        $star = $request->fecha_ini . ' ' . $request->hora_ini;
        $end = $request->fecha_fin . ' ' . $request->hora_fin;

        $movimientos = collect();

        $producto = Producto::findOrFail($id);

        $compras = Compra::with('detalles')
        ->whereBetween('created_at', array($star, $end))
        ->whereHas('detalles', function($query) use ($id)
        {
            $query->where('producto_id', $id);
        })
        ->get();


        foreach ($compras as $compra) {
            foreach ($compra->detalles as $detalle) {
                if($detalle->producto_id == $id)
                {
                    $movimientos->push([
                        'fecha'         => $compra->created_at,
                        'referencia'    => $compra->referencia,
                        'categoria'     => $detalle->producto()->first()->categoria,
                        'entrada'       => $detalle->cantidad,
                        'salida'        => null,
                        'usuario'       =>  $compra->usuario,
                        'tipo'          => 'Compra'
                    ]);
                }
            }
        }

        $devolucioncompras = DevolucionCompra::with('detalles')
        ->whereBetween('created_at', array($star, $end))
        ->whereHas('detalles', function($query) use ($id)
        {
            $query->where('producto_id', $id);
        })
        ->get();


        foreach ($devolucioncompras as $compra) {
            foreach ($compra->detalles as $detalle) {
                if($detalle->producto_id == $id)
                {
                    $movimientos->push([
                        'fecha'     => $compra->created_at,
                        'referencia'  => $compra->referencia,
                        'categoria' => $detalle->producto()->first()->categoria,
                        'entrada'       => null,
                        'salida'        => $detalle->cantidad,
                        'usuario'  =>  $compra->usuario,
                        'tipo'      => 'Devolución de Compra'
                    ]);
                }
            }
        }


        $requisiciones = Requisicion::with('detalles')
        ->whereBetween('created_at', array($star, $end))
        ->whereHas('detalles', function($query) use ($id)
        {
            $query->where('producto_id', $id);
        })
        ->get();

        foreach ($requisiciones as $requisicion) {
            foreach ($requisicion->detalles as $detalle) {
                if($detalle->producto_id == $id)
                {
                    $movimientos->push([
                        'fecha'     => $requisicion->created_at,
                        'referencia'  => $requisicion->id,
                        'categoria' => $detalle->producto()->first()->categoria,
                        'entrada'  => $detalle->cantidad,
                        'salida'  => $detalle->cantidad,
                        'usuario'  =>  $requisicion->usuario,
                        'tipo'      => 'Requisición a Sala de Venta'
                    ]);
                }
            }
        }

        $ajustes = Ajuste::where('producto_id', $id)
        ->whereBetween('created_at', array($star, $end))
        ->get();

        foreach ($ajustes as $ajuste) {
            if($ajuste->producto_id == $id)
            {
                $movimientos->push([
                    'fecha'     => $ajuste->created_at,
                    'referencia'  => $ajuste->id,
                    'categoria' => $ajuste->producto()->first()->categoria,
                    'entrada'       => $ajuste->ajuste > 0 ? $ajuste->ajuste : null,
                    'salida'        => $ajuste->ajuste < 0 ? $ajuste->ajuste : null,
                    'usuario'  =>  $ajuste->usuario,
                    'tipo'      => 'Ajuste: ' . $ajuste->lugar
                ]);
            }
        }


        $ventas = Venta::with('detalles')
        ->whereBetween('created_at', array($star, $end))
        ->whereHas('detalles', function($query) use ($id)
        {
            $query->where('producto_id', $id);
        })
        ->get();
        foreach ($ventas as $venta) {
            foreach ($venta->detalles as $detalle) {
                if($detalle->producto_id == $id)
                {
                    if ($venta->estado == 'Anulada') {
                        $tipo = 'Venta: Anulada';
                    }else{
                        $tipo = 'Venta';
                    }
                    $movimientos->push([
                        'fecha'     => $venta->created_at,
                        'referencia'  => $venta->correlativo ? $venta->correlativo : $venta->id,
                        'categoria' => $detalle->producto()->first()->categoria,
                        'entrada'  => null,
                        'salida'  => $detalle->cantidad,
                        'usuario'  =>  $venta->usuario,
                        'tipo'      => $tipo

                    ]);
                }
            }
        }

        $devolucionventas = DevolucionVenta::with('detalles')
        ->whereBetween('created_at', array($star, $end))
        ->whereHas('detalles', function($query) use ($id)
        {
            $query->where('producto_id', $id);
        })
        ->get();
        foreach ($devolucionventas as $venta) {
            foreach ($venta->detalles as $detalle) {
                if($detalle->producto_id == $id)
                {
                    $movimientos->push([
                        'fecha'     => $venta->created_at,
                        'referencia'  => $venta->correlativo ? $venta->correlativo : $venta->id,
                        'categoria' => $detalle->producto()->first()->categoria,
                        'entrada'  => $detalle->cantidad,
                        'salida'  => null,
                        'usuario'  =>  $venta->usuario,
                        'tipo'      => 'Devolución de Venta'

                    ]);
                }
            }
        }

        
        $producto->movimientos = $movimientos->sortByDesc('fecha')->values()->all();

        return Response()->json($producto, 200);
    }

}
