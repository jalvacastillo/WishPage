<?php

namespace App\Http\Controllers\Productos;

use App\Http\Controllers\Controller;
use App\Http\Requests\Productos\ComentarioRequest;
use App\Models\Productos\Comentario;

class ComentariosController extends Controller
{
    

    public function index($producto_id) {
       
        $comentarios = Comentario::where('producto_id', $producto_id)->with('cliente')->orderBy('created_at','desc')->get();

        return Response()->json($comentarios, 200);

    }


    public function read($id) {

        $comentario = Comentario::findOrFail($request->id);
        return Response()->json($comentario, 200);

    }


    public function store(ComentarioRequest $request)
    {
        if($request->id){
            $comentario = Comentario::findOrFail($request->id);
        }
        else{
            $comentario = new Comentario;
        }
        
        $comentario->fill($request->all());
        $comentario->save();

        return Response()->json($comentario, 200);

    }

    public function delete($id)
    {
        $comentario = Comentario::findOrFail($id);
        $comentario->delete();

        return Response()->json($comentario, 201);

    }


}
