<?php

namespace App\Http\Controllers\Productos;

use App\Http\Controllers\Controller;
use App\Http\Requests\Productos\CategoriaRequest;

use App\Models\Admin\Categoria;

class CategoriasController extends Controller
{
    

    public function index() {
       
        $categorias = Categoria::orderBy('created_at','dsc')->with('subcategorias')->paginate(7);

        return Response()->json($categorias, 200);

    }


    public function read($id) {

        $categoria = Categoria::findOrFail($id);
        return Response()->json($categoria, 200);

    }


    public function store(CategoriaRequest $request)
    {
        if($request->id){
            $categoria = Categoria::findOrFail($request->id);
        }
        else{
            $categoria = new Categoria;
        }     

        $categoria->fill($request->all());
        $categoria->save();        

        return Response()->json($categoria, 200);

    }

    public function delete($id)
    {
        $categoria = Categoria::findOrFail($id);
        $categoria->delete();

        return Response()->json($categoria, 201);

    }


    public function search($txt) {

        $categorias = Categoria::whereHas('cliente', function($query) use ($txt)
                    {
                        $query->where('nombre', 'like' ,'%' . $txt . '%');

                    })->paginate(7);

        return Response()->json($categorias, 200);

    }


}
