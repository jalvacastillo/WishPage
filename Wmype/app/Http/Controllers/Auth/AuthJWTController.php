<?php

namespace App\Http\Controllers\Auth;

use JWTAuth;
use App\User;
use Carbon\Carbon;
use App\Models\Admin\Empresa;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthJWTController extends Controller
{
    

    public function login(Request $request){

        $credentials = $request->only('email', 'password');
        $token = null;

        if (!$token = JWTAuth::attempt($credentials)) {
            return  Response()->json(['error' => 'Datos incorrectos', 'code' => 401], 401);
        }

        $user = JWTAuth::authenticate($token);

        if ($user->activo && $user->tipo == 'Administrador') {
            return response()->json(['token' => $token, 'user' => $user], 200);
        }else{
            return  Response()->json(['error' => 'El usuario no tiene acceso', 'code' => 401], 401);
        }


    }

    public function register(RegisterRequest $request){

        // Creamos primero el laboratorio vacio.
        $empresa = new Empresa;
        $empresa->nombre         = $request->empresa;
        // $empresa->vencimiento   = Carbon::now()->addMonths(1);
        $empresa->save();

        $user = new User;
        $user->name         = $request->name;
        $user->email        = $request->email;
        $user->password     = bcrypt($request->password);
        // $user->empresa_id     = $empresa->id;
        $user->save();

        if (!$token = JWTAuth::attempt(['email'=> $request->email, 'password' => $request->password])) {
            return  Response()->json(['error' => 'Datos incorrectos', 'code' => 401], 401);
        }

        $user = JWTAuth::authenticate($token);

        return response()->json(['token' => $token, 'user' => $user], 200);        

    }


}