<?php

namespace App\Models\Empresa;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Empresa extends Model
{
    use SoftDeletes;
    protected $table = 'empresa';
    protected $fillable = array(
        'logo',
        'nombre',
        'slug',
        'descripcion',
        'telefono',
        'correo',
        'municipio',
        'departamento',
        'sector',
        'whatsapp',
        'url_facebook',
        'url_instagram',
        'url_web',
        'usuario_id',
    );

    public function productos(){
        return $this->hasMany('App\Models\Productos\Producto', 'empresa_id');
    }
}
