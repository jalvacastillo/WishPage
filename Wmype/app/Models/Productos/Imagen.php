<?php

namespace App\Models\Productos;

use Illuminate\Database\Eloquent\Model;

class Imagen extends Model {

    protected $table = 'productos_imagenes';
    protected $fillable = array(
        'producto_id',
        'img',
        'orden'
    );

    public function producto(){
        return $this->belongsTo('App\Models\Productos\Producto','producto_id');
    }

}