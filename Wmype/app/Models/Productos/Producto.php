<?php

namespace App\Models\Productos;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Producto extends Model {

    protected $table = 'productos';
    protected $fillable = array(
        'nombre',
        'img',
        'descripcion',
        'slug',
        'precio',
        'categoria_id',
        'empresa_id'
    ); 

    //protected $appends = ['categoria', 'subcategoria', 'img', 'nuevo', 'reviews', 'rating'];


    // public function getCategoriaAttribute()
    // {
    //     return $this->subcategoria()->first()->categoria()->pluck('nombre')->first();
    // }

    // public function getSubcategoriaAttribute()
    // {
    //     return $this->subcategoria()->pluck('nombre')->first();
    // }

    // public function getImgAttribute()
    // {
    //     return $this->imagenes()->orderBy('orden', 'asc')->pluck('img')->first();
    // }

    // public function getNuevoAttribute()
    // {
    //     $today = Carbon::today();

    //     if ($this->created_at > $today->subMonth())
    //         return true;
    //     return false;
    // }

    // public function scopeExclusivos($query)
    // {
    //     return $query->where('exclusivo', true);
    // }

    // public function scopeNuevos($query)
    // {
    //     $today = Carbon::today();
    //     return $query->where('created_at', '>', $today->subMonth());
    // }

    // public function scopeOfertas($query)
    // {
    //     return $query->where('precio_oferta', '!=', '');
    // }

    // public function getReviewsAttribute()
    // {
    //     return $this->comentarios()->count();
    // }

    // public function getRatingAttribute()
    // {
    //     return round($this->comentarios()->avg('nota'),2);
    // }


    // public function imagenes(){
    //     return $this->hasMany('App\Models\Productos\Imagen');
    // }


    // public function relaciones(){
    //     return Producto::whereHas('subcategoria', function($query){
    //                                 $query->where('nombre', $this->subcategoria);
    //                             })->take(7)->get();

    // }

    // public function atributos(){
    //     return $this->hasMany('App\Models\Productos\Atributo');
    // }

    // public function comentarios(){
    //     return $this->hasMany('App\Models\Productos\Comentario');
    // }

    

}