<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
// Route::get('/producto/{id}', 'HomeController@producto')->name('producto');
Route::get('/buscador', 'HomeController@buscador')->name('buscador');

Route::name('carrito')->get('/carrito', function () { 
    return view('carrito.index');
});

Route::name('favoritos')->get('/favoritos', function () { 
    return view('favoritos.index');
});

// empresa
Route::get('/empresa/{slug}',             'HomeController@empresa'    )->name('empresa');
