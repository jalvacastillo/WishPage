    <?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaTable extends Migration {

    public function up()
    {
        Schema::create('empresa', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('nombre');
            $table->string('descripcion');
            $table->string('slug');
            $table->integer('telefono')->nullable();
            $table->string('correo')->nullable();
            $table->string('municipio');
            $table->string('departamento');
            $table->string('sector');
            $table->string('whatsapp');
            $table->string('url_facebook');
            $table->string('url_instagram')->nullable();
            $table->string('url_web')->nullable();
            $table->integer('usuario_id');
            $table->string('logo')->default('default.png');
          

            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('empresa');
    }

}
