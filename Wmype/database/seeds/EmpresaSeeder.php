<?php

use Illuminate\Database\Seeder;
use App\Models\Empresa\Empresa;

class EmpresaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            $faker = Faker\Factory::create();

             for($i = 1; $i <= 12 ; $i++)
        {
            $table = new Empresa;
            $table->nombre        = $faker->text(10);
            $table->logo        = 'empresas/' . $faker->numberBetween(1,4) . '.png';
            $table->slug            = Str::slug($table->nombre);
            $table->descripcion   = 'Somos una empresa de El Salvador. Buscamos ofrecerte lo mejor en ropa, zapatos y accesorios.';
            $table->municipio      = 'Ilobasco';
            $table->departamento      = 'Cabañas';
            $table->sector          = 'Alimentos';
            $table->whatsapp          = $faker->phoneNumber;
            $table->url_facebook        =   'https://facebook.com';
            $table->url_instagram       =   'https://instagram.com';
            $table->usuario_id          = '1';
            
            $table->save();
    }
  }
    
}
