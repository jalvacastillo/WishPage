<?php

use Illuminate\Database\Seeder;
use App\User;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

            $user = new User;
            $user->nombre     = 'Usuario Admin';
            $user->email    = 'admin@admin.com';
            $user->password = Hash::make('admin');
            $user->tipo     = 'Administrador';
            $user->save();

            $user = new User;
            $user->nombre = 'Jesus Alvarado';
            $user->email = 'cliente@cliente.com';
            $user->password = Hash::make('cliente');
            $user->tipo = 'Cliente';
            $user->save();

            // $table = new Direccion;
            // $table->nombre      = $faker->name;
            // $table->apellido    = $faker->lastname;
            // $table->pais        = $faker->country;
            // $table->estado      = $faker->state;
            // $table->ciudad      = $faker->state;
            // $table->linea1      = $faker->address;
            // $table->linea2      = $faker->address;
            // $table->zipcode     = $faker->postcode;
            // $table->telefono    = $faker->phonenumber;
            // $table->cliente_id  = 2;
            // $table->save();
            
    }
}
