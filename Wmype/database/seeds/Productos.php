<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str as Str;

use App\Models\Productos\Producto;
use App\Models\Productos\Imagen;


class Productos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i = 1; $i <= 50 ; $i++)
        {
            $table = new Producto;

            $table->nombre          = $faker->name;
            $table->descripcion     = $faker->text(24);
            $table->slug            = Str::slug($table->nombre);
            
            $table->categoria_id    = $faker->numberBetween(1,8);
            $table->empresa_id    = $faker->numberBetween(1,12);
            $table->save();

            // for($j = 0; $j <= 2 ; $j++)
            // {
            //     Imagen::create(['producto_id' => $i, 'img' => 'productos/' . $faker->numberBetween(1,2) . '.jpg', 'orden' => $j]);
            // }

        //     for($j = 1; $j <= 2 ; $j++)
        //     {
        //         $a = AT::find($j);
        //         Atributo::create(['producto_id' => $i, 'nombre' => $a->nombre]);

        //         foreach ($a->valores as $valor) {
        //             Valor::create(['atributo_id' => $j, 'nombre' => $valor->nombre]);
        //         }
        //     }

        //     for($j = 1; $j <= 10 ; $j++)
        //     {
        //         Comentario::create(['producto_id' => $i,'cliente_id' => 2, 'descripcion' => $faker->text, 'img' => 'default' . $faker->numberBetween(1,5) . '.jpg', 'nota' => $faker->numberBetween(1,5), 'created_at' => $faker->date]);
        //     }
            
        // }

            
    }
  }
}