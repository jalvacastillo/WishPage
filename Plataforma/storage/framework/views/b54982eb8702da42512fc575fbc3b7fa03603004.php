<!DOCTYPE html>
<html>
    <head>
        <title>Websis - 404</title>
        <link href="<?php echo e(url('css/materialize.min.css')); ?>" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <div class="row text-center">
                <br><br>
                <img src="/img/logotipo.svg" alt="Page not found" width="400">
                <hr>

                <h2 class="headline text-warning"> 404</h2>
                <h3> <i class="fa fa-warning text-warning"></i> Oops! Página no encontrada. </h3>
                <p> No podimos encontrar la página que buscabas. 
                Quieres regresar <a href="javascript:history.back();">atrás. </a>
                </p>
                <br>
                <form class="search-form">
                <div class="input-group">
                  <input type="text" name="search" class="form-control" placeholder="Buscar">
                  <div class="input-group-btn">
                    <button type="submit" name="submit" class="btn btn-danger btn-flat"><i class="fa fa-search"></i></button>
                  </div>
                </div>
                </form>
                <br>
            </div>
        </div>
    </body>
</html>
