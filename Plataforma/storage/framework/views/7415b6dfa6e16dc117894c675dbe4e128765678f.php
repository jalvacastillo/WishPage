<!doctype html>
<html lang="es">
    <head>
        <title>Wish - Inicio</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <meta name="description" content="Una plataforma de pedidos en linea, que incluye página web, tienda en linea y un sistema para inverntario">
        <meta name="keywords" content="pagina web, sistema, ventas, pedidos">

        <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
        <link rel="alternate" hreflang="es-SV" href="http://wish.websis.me/" />

        <!-- Styles -->
        <link rel="stylesheet" href="<?php echo e(asset('css/materialize.min.css')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('css/animated.min.css')); ?>"/>

        <link rel="stylesheet" href="<?php echo e(asset('css/style.css')); ?>"/>

        <?php echo $__env->yieldContent('css'); ?>


    </head>
    <body>

        <?php echo $__env->make('loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        
        <?php echo $__env->make('navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <?php echo $__env->yieldContent('content'); ?>

        <?php echo $__env->make('footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        
        <script src="<?php echo e(asset('js/classie.min.js')); ?>"></script>
        <script src="<?php echo e(asset('js/jquery.min.js')); ?>"></script>
        <script src="<?php echo e(asset('js/materialize.min.js')); ?>"></script>
        <script src="<?php echo e(asset('js/wow.min.js')); ?>"></script>
        <script src="<?php echo e(asset('js/app.js')); ?>"></script>

        <?php echo $__env->yieldContent('js'); ?>

    </body>
</html>

