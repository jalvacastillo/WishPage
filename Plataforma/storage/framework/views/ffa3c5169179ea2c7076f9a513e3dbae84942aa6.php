  <div class="parallax-container valign-wrapper scrollspy" id="contactos" style="height:370px;">
    <div class="section no-pad-bot">
      <div class="container">
        <div class="row center">
          <h5 class="header col s12 light">
              Escribenos tus comentarios o dudas
          </h5>
          <a href="#form" class="btn m-modal btn-floating btn-large waves-effect waves-light wow bounceIn" data-wow-duration="2000ms" data-wow-delay="1500ms">
            <i class="material-icons">send</i>
          </a>
        </div>
      </div>
    </div>
    <div class="parallax"><img src="<?php echo e(asset('/img/sol.png')); ?>" alt="Unsplashed background img 2"></div>
  </div>