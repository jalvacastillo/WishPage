  <div class="container scrollspy" id="conocenos">
    <div class="section">

      
      <div class="row center">
        <div class="col s12 m3">
          <div class="icon-block wow fadeIn" data-wow-duration="1000ms" data-wow-delay="1500ms">
            <i class="center orange-text text-darken-4">
                <i class="material-icons">flash_on</i>
            </i>
            <h5 class="center">Productividad</h5>
            <p class="light"> Ahorre tiempo y dinero con nuestro sistema web. </p>
          </div>
        </div>

        <div class="col s12 m3">
          <div class="icon-block wow fadeIn" data-wow-duration="1000ms" data-wow-delay="2000ms">
            <i class="center green-text text-darken-4">
                <i class="material-icons">group</i>
            </i>
            <h5 class="center">Clientes</h5>
            <p class="light"> Ofrecezca sus productos a todo el mundo 24/7. </p>
          </div>
        </div>

        <div class="col s12 m3">
          <div class="icon-block wow fadeIn" data-wow-duration="1000ms" data-wow-delay="2500ms">
            <i class="center yellow-text text-darken-4">
                <i class="material-icons">grade</i>
            </i>
            <h5 class="center">Uso</h5>
            <p class="light"> Es muy secillo, seguro y fácil de usar. </p>
          </div>
        </div>

        <div class="col s12 m3">
          <div class="icon-block wow fadeIn" data-wow-duration="1000ms" data-wow-delay="3000ms">
            <i class="center blue-text text-darken-4"><i class="material-icons">settings</i></i>
            <h5 class="center">Soporte</h5>            
            <p class="light"> Brindamos soporte especializado. </p>
          </div>
        </div>
      </div>
      <!--   Componentes   -->
      <div class="row">
        <div class="col s12">
          <h4 class="center section-heading">Incluye</h4>
        </div>
        <?php echo $__env->make('home.componentes', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>

    </div>
</div>