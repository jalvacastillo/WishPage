<footer class="page-footer teal">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">Compañia</h5>
            <p class="grey-text text-lighten-4">
            Somos un equipo que quiere brindar soluciones informaticas de alto nivel a un bajo costo para que los negocios crescan y aprovechen la técnologia de la mejor manera.
            </p>
        </div>
        <div class="col l3 s6">
          <h5 class="white-text">Empresa</h5>
          <ul>
            <li><a class="white-text m-modal" href="#politicas">Politicas</a></li>
            <li><a class="white-text m-modal" href="#terminos">Términos de usos</a></li>
            <li><a class="white-text m-modal" href="#sitemap">Mapa del Sitio</a></li>
          </ul>
        </div>
        <div class="col l3 s6">
          <h5 class="white-text">Información</h5>
          <ul>
            <li><a class="white-text" href="#!">Ilobasco, Cabañas El Salvador</a></li>
            <li><a class="white-text" href="#!">(+503) 7891-2933</a></li>
            <li><a class="white-text" href="#!">Página oficial</a></li>
          </ul>
        </div>
      </div>
    </div>

    <div class="footer-copyright">
      <div class="container center">
      Copyright &copy 2016 | 
      <a class="brown-text text-lighten-3" href="#">by Websis Inc.</a>
      </div>
    </div>

</footer>