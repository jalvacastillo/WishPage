<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('home.slider', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <?php echo $__env->make('home.conocenos', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    
    <?php echo $__env->make('home.funcionamiento', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <?php echo $__env->make('home.planes', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <?php echo $__env->make('home.contactos', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <?php echo $__env->make('home.partials.politicas', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('home.partials.sitemap', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('home.partials.terminos', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('home.partials.form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>