<nav class="menu white" role="navigation">
    <div class="nav-wrapper container">
        
        <a id="logo-container" href="<?php echo e(url('/')); ?>" class="brand-logo">
            <img src="<?php echo e(asset('/img/logo.png')); ?>" alt="Logotipo" width="30">
            Wishpage
        </a>

        <ul class="right hide-on-med-and-down table-of-contents">

            <li> <a class="page-scroll waves-effect waves-light" href="#index-banner"> Inicio </a> </li>
            <li> <a class="page-scroll waves-effect waves-light" href="#conocenos"> Conocenos </a> </li>
            <li> <a class="page-scroll waves-effect waves-light" href="#funcionamiento"> Funcionamiento </a> </li>
            <li> <a class="page-scroll waves-effect waves-light" href="#planes">Planes </a> </li>
            <li> <a class="page-scroll waves-effect waves-light" href="#contactos">Contactos </a> </li>
            <li> <a class="waves-effect waves-light" href="<?php echo e(url('tiendas')); ?>">Tiendas </a> </li>
            <li> <a href="<?php echo e(url('login')); ?>"> Entrar </a> </li>

        </ul>

        <ul id="nav-mobile" class="side-nav">

            <li> <a class="page-scroll waves-effect waves-light" href="#index-banner"> Inicio </a> </li>
            <li> <a class="page-scroll waves-effect waves-light" href="#ventajas"> Ventajas </a> </li>
            <li> <a class="page-scroll waves-effect waves-light" href="#funcionamiento"> Funcionamiento </a> </li>
            <li> <a class="page-scroll waves-effect waves-light" href="#planes"> Planes </a> </li>
            <li> <a class="page-scroll waves-effect waves-light" href="#contactos"> Contactos </a> </li>
            <li> <a class="waves-effect waves-light" href="<?php echo e(url('tiendas')); ?>">Tiendas </a> </li>
            <li> <a href="<?php echo e(url('login')); ?>"> Entrar </a> </li>

        </ul>

        <a href="#" data-activates="nav-mobile" class="button-collapse">
            <i class="material-icons">menu</i>
        </a>
        
    </div>
</nav>