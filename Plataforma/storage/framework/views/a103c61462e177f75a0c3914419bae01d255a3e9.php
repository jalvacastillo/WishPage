<!-- Modal Structure -->
<div id="form" class="modal modal-fixed-footer row" ng-controller="FormCtrl">
  <form role="form" method="POST" ng-submit="email(correo);">
        <?php echo csrf_field(); ?>

      <div class="modal-content">
        <h4 class="section-heading center">Gracias por escribirnos</h4>

            <div class="input-field col s12">
                <input id="nombre" type="text" class="validate" required ng-model="correo.nombre">
                <label for="nombre">Tu nombre</label>
            </div>
            
            <div class="input-field col s12">
              <input id="email" name="email" type="email" class="validate" required ng-model="correo.email">
              <label for="email" name="email">Tu correo</label>
            </div>
            
            <div class="input-field col s12">
                <textarea id="mensaje" class="materialize-textarea" required ng-model="correo.mensaje"></textarea>
                <label for="mensaje">Dinos en que te ayudamos</label>
            </div>

      </div>
      <div class="modal-footer">
            <button class="right modal-action waves-effect waves-light btn" type="submit" name="action">
              <span ng-show="!loader">Enviar</span>
              <span ng-show="loader">Enviando...</span>
            </button>
            <a href="#!" class="left modal-close waves-effect waves-green btn-flat ">Cancelar</a>
      </div>
    </form>
</div>
          