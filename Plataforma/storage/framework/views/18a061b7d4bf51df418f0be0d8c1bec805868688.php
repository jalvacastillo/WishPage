<div class="slider scrollspy" id="index-banner">
    <ul class="slides">
      <li>
        <img src="<?php echo e(asset('/img/slider/one.svg')); ?>">
          <div class="caption center-align">
            <h2 class="teal-text text-darken-2">Wish Page</h2>
            <h4 class="teal-text text-light-2">Página + Tienda + Sistema</h4>
            <div class="row">
              <a href="#conocenos" class="btn-large waves-effect waves-light teal darken-1 wow fadeInUp page-scroll" data-wow-duration="2000ms" data-wow-delay="1000ms">
                Conoce más
              </a>
            </div>
          </div>
      </li>
      <li>
        <img src="<?php echo e(asset('/img/slider/one.svg')); ?>"> <!-- random image -->
        <div class="caption left-align">
          <h2 class="teal-text text-darken-2">Es seguro y fácil de usar</h2>
          <h4 class="teal-text text-light-2">Todo incluido en una sola plataforma</h4>
          <div class="row">
            <a href="#funcionamiento" class="btn-large waves-effect waves-light teal darken-1 page-scroll">
              Mira como funciona
            </a>
          </div>
        </div>
      </li>
      <li>
        <img src="<?php echo e(asset('/img/slider/one.svg')); ?>"> <!-- random image -->
        <div class="caption right-align">
          <h2 class="teal-text text-darken-2">Pruebalo grátis</h2>
          <h4 class="teal-text text-light-2">Te ofrecemos un mes sin costo</h4>
          <div class="row">
            <a href="#planes" class="btn-large waves-effect waves-light teal darken-1 page-scroll">
              Mira nuestros planes
            </a>
          </div>
        </div>
      </li>
    </ul>
  </div>