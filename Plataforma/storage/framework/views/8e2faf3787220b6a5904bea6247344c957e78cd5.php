<div id="funcionamiento" class="parallax-container valign-wrapper scrollspy" style="height:650px;">
<div class="section">
  <div class="container">
    <div class="row center">
      <h4 class="header col s12 light">
          Mira como funciona Wishpage
      </h4>
      <a href="#video" class="btn m-modal btn-floating btn-large waves-effect waves-light wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="500ms">
        <i class="material-icons">play_arrow</i>
      </a>
      <!-- Modal Structure -->
      <div id="video" class="modal">
          <video class="responsive-video" controls poster="<?php echo e(asset('/img/sol.png')); ?>">
          <source src="movie.mp4" type="video/mp4">
          </video>
      </div>
    </div>
  </div>
</div>
<div class="parallax"><img src="<?php echo e(asset('/img/kite.svg')); ?>" alt="Unsplashed background img 2"></div>
</div>