
    <div class="card col s12 m4 wow fadeInLeft" data-wow-duration="1000ms" data-wow-delay="500ms">
        <div class="card-image waves-effect waves-block waves-light">
          <img class="activator" src="<?php echo e(asset('/img/servicios/pagina.jpg')); ?>">
        </div>
        <div class="card-content">
          <span class="card-title activator grey-text text-darken-4">Página Web<i class="material-icons right">more_vert</i></span>
        </div>
        <div class="card-reveal">
          <span class="card-title grey-text text-darken-4">Página Web<i class="material-icons right">close</i></span>
            <p class="justify">
            Tendrá una herramienta que le permitirá dar a conocer sus productos y servicios a todo el mundo.
            </p>
        </div>
    </div>


    <div class="card col s12 m4 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="500ms">
        <div class="card-image waves-effect waves-block waves-light">
          <img class="activator" src="<?php echo e(asset('/img/servicios/tienda.png')); ?>">
        </div>
        <div class="card-content">
          <span class="card-title activator grey-text text-darken-4">Tienda en Linea<i class="material-icons right">more_vert</i></span>
        </div>
        <div class="card-reveal">
          <span class="card-title grey-text text-darken-4">Tienda en Linea<i class="material-icons right">close</i></span>
            <p>
            Contará con una sucursal de su negoció abierta al mundo y disponible a cualquier hora todos los dias.
            </p>
        </div>
    </div>


    <div class="card col s12 m4 wow fadeInRight" data-wow-duration="1000ms" data-wow-delay="500ms">
        <div class="card-image waves-effect waves-block waves-light">
          <img class="activator" src="<?php echo e(asset('/img/servicios/sistema.jpg')); ?>">
        </div>
        <div class="card-content">
          <span class="card-title activator grey-text text-darken-4">Sistema web<i class="material-icons right">more_vert</i></span>
        </div>
        <div class="card-reveal">
          <span class="card-title grey-text text-darken-4">Sistema Web<i class="material-icons right">close</i></span>
            <p>
            Ahorrará tiempo, dinero y trabajo llevando el control de sus productos, pedidos y clientes.
            </p>
        </div>
    </div>