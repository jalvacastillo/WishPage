
<div class="container scrollspy" id="planes">
    <div class="section">
      <div class="row">
        <div class="col s12">
          <h4 class="section-heading center">Planes</h4>
            
            <div class="col l4 s12 card hoverable">
              <ul class="collection with-header">
                <li class="collection-header center blue"><h4>$10 <small>Mensual</small></h4></li>
                <li class="collection-item"> 
                  <i class="material-icons blue-text left">check_circle</i> 
                  Página Web
                </li>
                <li class="collection-item"> 
                  <i class="material-icons blue-text left">check_circle</i> 
                  Tienda en Linea
                </li>
                <li class="collection-item"> 
                  <i class="material-icons blue-text left">check_circle</i> 
                  Control de Inventario
                </li>
                <li class="collection-item"> 
                  <i class="material-icons blue-text left">check_circle</i> 
                  Control de Pedidos
                </li>
                <li class="collection-item"> 
                  <i class="material-icons blue-text left">check_circle</i> 
                  Soporte Gratis
                </li>
              </ul>
              <p class="center">
                <a href="<?php echo e(url('register')); ?>" class="btn-large waves-effect waves-light teal lighten-1"> Adquirir </a>
              </p>
            </div>

            
            <div class="col l4 s12 card hoverable" style="z-index:1;">
              <ul class="collection with-header">
                <li class="collection-header center yellow"><h3>$0 <small>Gratis</small></h3></li>
                <li class="collection-item"> 
                  <i class="material-icons blue-text left">check_circle</i> 
                  Página Web
                </li>
                <li class="collection-item"> 
                  <i class="material-icons blue-text left">check_circle</i> 
                  Tienda en Linea
                </li>
                <li class="collection-item"> 
                  <i class="material-icons blue-text left">check_circle</i> 
                  Control de Inventario
                </li>
                <li class="collection-item"> 
                  <i class="material-icons blue-text left">check_circle</i> 
                  Control de Pedidos
                </li>
                <li class="collection-item"> 
                  <i class="material-icons blue-text left">check_circle</i> 
                  Soporte Gratis
                </li>
                <li class="collection-item"> 
                  <i class="material-icons blue-text left">check_circle</i> 
                  Un mes de prueba
                </li>
              </ul>
              <p class="center">
                <a href="<?php echo e(url('register')); ?>" class="btn-large waves-effect waves-light teal lighten-1"> Adquirir </a>
              </p>
            </div>

            
            <div class="col l4 s12 card hoverable">
              <ul class="collection with-header">
                <li class="collection-header center orange"><h4>$80 <small>Anual</small></h4></li>
                <li class="collection-item"> 
                  <i class="material-icons blue-text left">check_circle</i> 
                  Página Web
                </li>
                <li class="collection-item"> 
                  <i class="material-icons blue-text left">check_circle</i> 
                  Tienda en Linea
                </li>
                <li class="collection-item"> 
                  <i class="material-icons blue-text left">check_circle</i> 
                  Control de Inventario
                </li>
                <li class="collection-item"> 
                  <i class="material-icons blue-text left">check_circle</i> 
                  Control de Pedidos
                </li>
                <li class="collection-item"> 
                  <i class="material-icons blue-text left">check_circle</i> 
                  Soporte Gratis
                </li>
              </ul>
              <p class="center">
                <a href="<?php echo e(url('register')); ?>" class="btn-large waves-effect waves-light teal lighten-1"> Adquirir </a>
              </p>
            </div>
        </div>
      </div>
    </div>
</div>